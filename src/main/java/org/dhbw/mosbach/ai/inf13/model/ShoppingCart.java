package org.dhbw.mosbach.ai.inf13.model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Klasse für Shopping Cart Entität.
 *
 */
@Entity

public class ShoppingCart implements Serializable {

	private long id;
	private String name;
	private String brand;
	private double price;
	private long userid;

	private static final long serialVersionUID = 1L;

	public ShoppingCart() {
		super();
	}

	@Id
	@GeneratedValue
	@XmlTransient
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getuserid() {
		return userid;
	}

	public void setuserid(long Userid) {
		userid = Userid;
	}

}
