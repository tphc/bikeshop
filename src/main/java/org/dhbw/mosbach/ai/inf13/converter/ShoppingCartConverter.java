package org.dhbw.mosbach.ai.inf13.converter;

import javax.faces.convert.FacesConverter;

import org.dhbw.mosbach.ai.inf13.db.BaseDao;
import org.dhbw.mosbach.ai.inf13.db.ShoppingCartDao;
import org.dhbw.mosbach.ai.inf13.model.ShoppingCart;

/**
 * Konverter für Shopping Cart.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@FacesConverter(forClass = ShoppingCart.class, value = "shoppingCart")
public class ShoppingCartConverter extends LongIdEntityConverter<ShoppingCart> {
	@Override
	protected Class<? extends BaseDao<ShoppingCart, Long>> getBaseDaoClass() {
		return ShoppingCartDao.class;
	}
}
