package org.dhbw.mosbach.ai.inf13.beans;

import java.io.Serializable;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.dhbw.mosbach.ai.inf13.db.BikeDao;
import org.dhbw.mosbach.ai.inf13.model.Bike;
import org.dhbw.mosbach.ai.inf13.model.Roles;

/**
 * Diese Klasse wird verwendet, wenn ein neues Bike angelegt, oder ein altes
 * bearbeitet wird.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@Named
@SessionScoped
@RolesAllowed(value = { Roles.ADMIN })
public class EditBikeBean implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final String EDIT_BIKE_VIEW = "editbike";

	@Inject
	private BikeDao BikeDao;

	private Bike Bike;
	private boolean isNew = false;

	public String newBike() {
		this.Bike = new Bike();
		this.isNew = true;

		return EDIT_BIKE_VIEW;
	}

	public String edit(Bike Bike) {
		this.Bike = BikeDao.findById(BikeDao.getId(Bike));
		this.isNew = false;

		return EDIT_BIKE_VIEW;
	}

	public String save() {
		if (isNew) {
			BikeDao.persist(Bike);
			isNew = false;
		} else {
			BikeDao.merge(Bike);
		}

		return "home";
	}

	public Bike getBike() {
		return this.Bike;
	}

	public void setBike(Bike Bike) {
		this.Bike = Bike;
	}
}
