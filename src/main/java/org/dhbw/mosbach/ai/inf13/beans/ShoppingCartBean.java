package org.dhbw.mosbach.ai.inf13.beans;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.dhbw.mosbach.ai.inf13.db.ShoppingCartDao;
import org.dhbw.mosbach.ai.inf13.model.ShoppingCart;
import org.dhbw.mosbach.ai.inf13.model.Bike;

/**
 * Dieser Bean verwaltet die Funktionen des Shopping Cart.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@Named
@SessionScoped
public class ShoppingCartBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String ShoppingCart_VIEW = "shoppingcart";

	@Inject
	private ShoppingCartDao ShoppingCartDao;

	@Inject
	private SecurityBean SecurityBean;

	private ShoppingCart ShoppingCart;

	@PersistenceContext
	private EntityManager em;

	public ShoppingCart getShoppingCart() {
		return this.ShoppingCart;
	}

	public void setShoppingCart(ShoppingCart ShoppingCart) {
		this.ShoppingCart = ShoppingCart;
	}

	/**
	 * Diese Methode fügt dem Shopping Cart einen neuen Artikel hinzu.
	 * 
	 * @param bike
	 *            Das bike, dass dem Cart hinzugefügt wird.
	 * @return String ShoppingCart_VIEW , um danach den Shopping Cart zu sehen.
	 */

	public String addToCart(Bike bike) {

		final ShoppingCart shoppingcart = createShoppingCart(bike.getName(), bike.getPrice(), bike.getBrand(),
				SecurityBean.getUser().getId());

		ShoppingCartDao.persist(shoppingcart);

		return ShoppingCart_VIEW;
	}

	private ShoppingCart createShoppingCart(String name, double price, String brand, long userid) {
		final ShoppingCart shoppingcart = new ShoppingCart();
		shoppingcart.setName(name);
		shoppingcart.setPrice(price);
		shoppingcart.setBrand(brand);
		shoppingcart.setuserid(userid);
		return shoppingcart;
	}

	/**
	 * Diese Methode summiert die Shopping Cart Artikel eines Kunden.
	 * 
	 * @return Die Summe der Artikel wird ausgegeben. Sollten keine Artikel im
	 *         Cart sein, wird 0 ausgegeben.
	 */

	public double getCartValue() {
		long id = SecurityBean.getUser().getId();

		final String query = String.format("SELECT SUM(s.price) FROM ShoppingCart s WHERE userid=" + id,
				ShoppingCart.class);
		@SuppressWarnings("unchecked")
		List<Object> cartValueList = em.createQuery(query).getResultList();

		if (cartValueList.get(0) == null) {
			return (double) 0;
		}
		return (double) cartValueList.get(0);
	}

	/**
	 * Diese Methode löscht einen Artikel aus dem Shopping Cart.
	 * 
	 * @param ShoppingCart
	 *            Der zu löschende Artikel.
	 * @return String ShoppingCart_VIEW , um danach den Shopping Cart zu sehen.
	 * 
	 */

	@Transactional
	public String deleteShoppingCart(ShoppingCart ShoppingCart) {
		this.ShoppingCart = ShoppingCartDao.findById(ShoppingCartDao.getId(ShoppingCart));

		long id = ShoppingCart.getId();

		final String query = String.format("DELETE FROM ShoppingCart s WHERE s.id=" + id, ShoppingCart.class);

		em.createQuery(query).executeUpdate();

		return ShoppingCart_VIEW;
	}

	public void setCartValue(double cartValue) {
	}
}
