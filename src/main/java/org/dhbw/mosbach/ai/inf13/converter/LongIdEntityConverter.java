package org.dhbw.mosbach.ai.inf13.converter;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.dhbw.mosbach.ai.inf13.db.BaseDao;

public abstract class LongIdEntityConverter<E> implements Converter {
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return getEntityById(Long.valueOf(value));
	}

	protected E getEntityById(Long id) {
		return getBaseDao().findById(id);
	}

	protected BaseDao<E, Long> getBaseDao() {
		return CDI.current().select(getBaseDaoClass()).get();
	}

	/**
	 * @return {@link BaseDao} class for entity E
	 */
	protected abstract Class<? extends BaseDao<E, Long>> getBaseDaoClass();

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}

		final Number id = getBaseDao().getId((E) value);

		return String.valueOf(id);
	}
}
