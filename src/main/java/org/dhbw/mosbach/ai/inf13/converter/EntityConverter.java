package org.dhbw.mosbach.ai.inf13.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.dhbw.mosbach.ai.inf13.db.Tools;

@FacesConverter(value = "entity")
public class EntityConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Object result = null;

		if (value != null) {
			final int atPos = value.indexOf("@");

			if (atPos != -1) {
				final String className = value.substring(0, atPos);
				final Long id = Long.decode(value.substring(atPos + 1));

				final Class<?> clazz;

				try {
					clazz = Thread.currentThread().getContextClassLoader().loadClass(className);
				} catch (final ClassNotFoundException e) {
					throw new IllegalArgumentException("unknown class", e);
				}

				result = Tools.getEntityManager().find(clazz, id);
			}
		}

		return result;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}

		return String.format("%s@%s", value.getClass().getName(), Tools.getEntityKey(value));
	}
}
