package org.dhbw.mosbach.ai.inf13.ejb.aspects;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Interceptor
public class MethodProtocolInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(MethodProtocolInterceptor.class);

	@AroundInvoke
	@AroundTimeout
	public Object protocolMethod(InvocationContext ctx) throws Exception {
		logger.info("::Protocol>Method [{}] with [{}] parameters...", ctx.getMethod().getName(),
				ctx.getParameters().length);
		final Object result = ctx.proceed();
		logger.info("...done invoking method.");
		return result;
	}

	// @AroundConstruct
	// public Object protocolConstruct(InvocationContext ctx) throws Exception {
	// logger.info("::Protocol>Construct...");
	// return ctx.proceed();
	// }

	@PostConstruct
	private void protocolPostConstruct(InvocationContext ctx) throws Exception {
		logger.info("::Protocol>PostContruct...");
		ctx.proceed();
	}

	@PreDestroy
	private void protocolPreDestroy(InvocationContext ctx) throws Exception {
		logger.info("::Protocol>PreDestroy...");
		ctx.proceed();
	}

}
