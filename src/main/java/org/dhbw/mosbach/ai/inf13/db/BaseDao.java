package org.dhbw.mosbach.ai.inf13.db;

import java.io.Serializable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

/**
 * 
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */
public abstract class BaseDao<E, I> implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	protected EntityManager em;

	protected final Class<?> entityClass;

	public BaseDao() {
		final TypeToken<E> type = new TypeToken<E>(getClass()) {
		};

		entityClass = type.getRawType();
	}

	@Transactional
	public void persist(E entity) {
		em.persist(entity);
	}

	@Transactional
	public void persist(@SuppressWarnings("unchecked") E... entities) {
		for (final E entity : entities) {
			persist(entity);
		}
	}

	@Transactional
	public void remove(E entity) {
		em.remove(entity);
	}

	@Transactional
	public E merge(E entity) {
		return em.merge(entity);
	}

	/**
	 * Tries to find the entity for the given id.
	 *
	 * @param id
	 *            id
	 * @return entity or null if no matching entity found
	 */
	@SuppressWarnings("unchecked")
	public E findById(I id) {
		return (E) em.find(entityClass, id);
	}

	/**
	 * Tries to find an instance in the db by using the given key.
	 *
	 * @param fieldName
	 *            field to search the key
	 * @param key
	 *            key to search
	 * @return entity or null if none found
	 */
	public E findByUnique(String fieldName, Object key) {
		final String query = String.format("FROM %s e WHERE e.%s = :key", entityClass.getName(), fieldName);

		@SuppressWarnings("unchecked")
		final List<E> resultList = em.createQuery(query).setParameter("key", key).getResultList();

		return resultList.isEmpty() ? null : resultList.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<E> getAll() {
		final String query = String.format("FROM %s e", entityClass.getName());

		return em.createQuery(query).getResultList();
	}

	/**
	 * Returns a list sorted by the given function. The list will be sorted by
	 * Java, so this is an expensive operation for big lists.
	 *
	 * @param stringFieldExtractor
	 *            extractor function, e.g. <code>Entity::getName</code>
	 * @return sorted list.
	 */
	public List<E> getAllSortedBy(Function<E, String> stringFieldExtractor) {
		final List<E> sortedList = Lists.newArrayList(getAll());
		Collections.sort(sortedList, Comparator.comparing(stringFieldExtractor, String.CASE_INSENSITIVE_ORDER));

		return sortedList;
	}

	/**
	 * Determines the primary key of the given entity by using reflection.
	 *
	 * @param e
	 *            entity
	 * @return primary key of the given entity
	 */
	public I getId(E e) {
		return Tools.<I> getEntityKey(e);
	}

	/**
	 * Tries to find a managed entity corresponding to the given (possibly
	 * detached) entity. If the entity is already managed, it will be returned
	 * immediately without Db lookup, so it is safe to call this function.
	 * <strong> Please be aware that any modifications to the given entity will
	 * not be reflected by the returned entity, if it has to be re-loaded from
	 * the persistence context!</strong>
	 *
	 * @param e
	 *            entity
	 * @return managed entity
	 */
	@Transactional
	public E reattach(E e) {
		return em.contains(e) ? e : findById(getId(e));
	}
}
