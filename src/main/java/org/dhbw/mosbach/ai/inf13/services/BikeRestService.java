package org.dhbw.mosbach.ai.inf13.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.dhbw.mosbach.ai.inf13.db.BikeDao;
import org.dhbw.mosbach.ai.inf13.model.Bike;

/**
 * REST Webservice, um Demodaten zu erzeugen und Bike Daten als JSON auszugeben.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@ApplicationScoped
@Path("/bike")
public class BikeRestService {
	@Inject
	private BikeDao bikeDao;

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Gibt auf einen GET Request Daten der Bike Entität als JSON Text aus.
	 * 
	 * @return JSON Text
	 */

	@GET
	@Path("/alljson")
	@Produces("text/json")
	public Bike[] getAllBikesJson() {
		final List<Bike> allBikes = bikeDao.getAll();

		return allBikes.toArray(new Bike[allBikes.size()]);
	}

	/**
	 * Erzeugt auf einen GET Request ein Demo Bike Objekt in der Datenbank.
	 *
	 */

	@GET
	@Path("createdemodata")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public void createDemoData() {
		final Bike bike = new Bike();
		bike.setName("Test Name");
		bike.setPrice(999);
		bike.setBrand("Test Brand");

		bikeDao.persist(bike);

	}
}
