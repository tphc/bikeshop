package org.dhbw.mosbach.ai.inf13.converter;

import javax.faces.convert.FacesConverter;

import org.dhbw.mosbach.ai.inf13.db.BikeDao;
import org.dhbw.mosbach.ai.inf13.db.BaseDao;
import org.dhbw.mosbach.ai.inf13.model.Bike;

/**
 * Konverter für Bike.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@FacesConverter(forClass = Bike.class, value = "Bike")
public class BikeConverter extends LongIdEntityConverter<Bike> {
	@Override
	protected Class<? extends BaseDao<Bike, Long>> getBaseDaoClass() {
		return BikeDao.class;
	}
}
