package org.dhbw.mosbach.ai.inf13.ejb.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.dhbw.mosbach.ai.inf13.ejb.aspects.MethodProtocolInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@Interceptors(MethodProtocolInterceptor.class)
public class InterceptedBean {
	private static final Logger logger = LoggerFactory.getLogger(InterceptedBean.class);

	public InterceptedBean() {
		logger.info("Stateless>Construct");
	}

	@PostConstruct
	private void init() {
		logger.info("Stateless>PostConstruct");
	}

	public void interceptedMethod() {
		logger.info("Stateless>InterceptedMethod");
		nonInterceptedMethod();
	}

	public void nonInterceptedMethod() {
		logger.info("Stateless>NonInterceptedMethod");
	}

	@PreDestroy
	private void destroy() {
		logger.info("Stateless>PreDestroy");
	}

}
