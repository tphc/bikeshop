package org.dhbw.mosbach.ai.inf13.db;

import org.dhbw.mosbach.ai.inf13.model.Bike;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 * DAO für die Bike Entität.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@Named("bikeDao")
@Dependent
public class BikeDao extends BaseDao<Bike, Long>
{
    public BikeDao()
    {
        super();
    }
}
