package org.dhbw.mosbach.ai.inf13.beans;

import java.io.Serializable;
import java.util.Arrays;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;

import org.dhbw.mosbach.ai.inf13.ejb.beans.AppUserDaoProxy;
import org.dhbw.mosbach.ai.inf13.model.AppRole;
import org.dhbw.mosbach.ai.inf13.model.AppUser;

/**
 * Mit diesem Bean kann sich ein Kunde einen neuen User anlegen.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@ManagedBean(name = "manageuser")
@SessionScoped
public class ManageUserBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private AppUserDaoProxy appUserDaoProxy;

	private AppUser AppUser;

	@Resource
	private UserTransaction userTransaction;

	@PersistenceContext
	private EntityManager em;

	/**
	 * Diese Methode fügt einen neuen Benutzer hinzu, indem der Benutzername und
	 * das Passwort von der Oberfläche genommen werden. Danach wird die customer
	 * Rolle aus der Datenbank, weil sie für das Anlegen eines neuen User
	 * Objektes benötigt wird. Die Idee mit der Rolle aus der Datenbank kam von
	 * Oliver Raum.
	 * 
	 * @param inputUser
	 *            Der eingegebene Benutzername.
	 * @param inputPassword
	 *            Das eingegebene Paswort.
	 * @throws SecurityException
	 * @throws IllegalStateException
	 * @throws RollbackException
	 * @throws HeuristicMixedException
	 * @throws HeuristicRollbackException
	 * @throws SystemException
	 * @throws NotSupportedException
	 * 
	 * @return String "home", um danach auf die Homeseite zu kommen.
	 */

	@Transactional
	public String addUser(String inputUser, String inputPassword)
			throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException,
			HeuristicRollbackException, SystemException, NotSupportedException {

		final Object customerRole = em.createQuery("SELECT s FROM AppRole s WHERE s.name = :role")
				.setParameter("role", "customer").getSingleResult();
		createUser(inputUser, "Customer " + inputUser, inputPassword, (AppRole) customerRole);

		return "home";
	}

	private AppUser createUser(String login, String userName, String password, AppRole... userRoles) {
		final AppUser user = new AppUser(login, userName);
		user.getRoles().addAll(Arrays.asList(userRoles));
		appUserDaoProxy.changePassword(user, password);
		appUserDaoProxy.persist(user);

		return user;
	}

	public AppUser getAppUser() {
		return AppUser;
	}

	public void setAppUser(AppUser appUser) {
		AppUser = appUser;
	}

}
