package org.dhbw.mosbach.ai.inf13.db;

import org.dhbw.mosbach.ai.inf13.beans.SecurityBean;
import org.dhbw.mosbach.ai.inf13.model.ShoppingCart;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * DAO für die Shopping Cart Entität.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@SuppressWarnings("serial")
@Named("shoppingCartDao")
@Dependent
public class ShoppingCartDao extends BaseDao<ShoppingCart, Long> {
	@Inject
	private SecurityBean SecurityBean;
	private List<ShoppingCart> ShoppingCartByUserID;

	public ShoppingCartDao() {
		super();
	}

	/**
	 * Diese Methode gibt eine Liste der Shopping Cart Artikel für einen User
	 * aus.
	 * 
	 * @return Sollte kein Artikel im Cart sein, wird ein neuer Cart Artikel
	 *         erstellt, mit einer Benachrichtigung. Dieser Artikel kommt aber
	 *         nicht in die Datenbank.
	 */

	@SuppressWarnings("unchecked")
	public List<ShoppingCart> getShoppingCartByUserID() {
		final long userid = SecurityBean.getUser().getId();
		final String query = String.format("SELECT s FROM ShoppingCart s where userid = " + userid, ShoppingCart.class);
		ShoppingCartByUserID = em.createQuery(query).getResultList();
		if (ShoppingCartByUserID.isEmpty()) {
			ShoppingCart sc = new ShoppingCart();
			sc.setName("There are no items in the shopping cart");
			ShoppingCartByUserID.add(sc);
		}
		return ShoppingCartByUserID;
	}

}
