package org.dhbw.mosbach.ai.inf13.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class BikeApplication extends Application {
}
