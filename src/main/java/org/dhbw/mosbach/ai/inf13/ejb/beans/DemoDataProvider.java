package org.dhbw.mosbach.ai.inf13.ejb.beans;

import java.security.Principal;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import org.dhbw.mosbach.ai.inf13.db.BikeDao;
import org.dhbw.mosbach.ai.inf13.model.AppRole;
import org.dhbw.mosbach.ai.inf13.model.AppUser;
import org.dhbw.mosbach.ai.inf13.model.Bike;
import org.dhbw.mosbach.ai.inf13.model.Roles;
import org.jboss.weld.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Die DemoDataProvider Klasse produziert Demo Daten, wenn die Datenbank leer
 * ist.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */

@Startup
@Singleton
@RunAs(Roles.ADMIN)
public class DemoDataProvider {
	private static final Logger logger = LoggerFactory.getLogger(DemoDataProvider.class);

	@PersistenceContext
	private EntityManager em;

	@Resource
	private EJBContext context;

	@Resource
	private TimerService timerService;

	@EJB
	private AppUserDaoProxy appUserDaoProxy;

	@Inject
	private ApplicationContext sessionContext;

	@Inject
	private BikeDao bikeDao;

	@PostConstruct
	private void init() {
		// unfortunately, security lookups are not allowed during postconstruct
		// of a
		// singleton,
		// so we have to delay this stuff...
		timerService.createSingleActionTimer(1000, new TimerConfig("ddp", false));
	}

	@Timeout
	@Transactional(TxType.REQUIRED)
	private void timer() {
		logger.info("In DemoDataProvider.init");

		final Principal callerPrincipal = context.getCallerPrincipal();
		final boolean isAdmin = context.isCallerInRole(Roles.ADMIN);
		logger.info("Principal: {}, admin: {}", callerPrincipal, Boolean.valueOf(isAdmin));

		// Check whether any data exists

		final Long bikeCount = (Long) em.createQuery("SELECT COUNT(b) FROM Bike b").getSingleResult();
		final Long userCount = (Long) em.createQuery("SELECT COUNT(au) FROM AppUser au").getSingleResult();
		logger.info("There are {} bikes and {} users in the DB.", bikeCount, userCount);

		if (userCount.longValue() == 0) {
			createUsers();
		}

		if (bikeCount.longValue() == 0) {
			createDemoData();
		}
	}

	/**
	 * Creates Demo Ingredients and Articles.
	 */
	@Transactional(TxType.REQUIRED)
	private void createDemoData() {
		System.out.println("Creating demo demoData");

		final Bike[] bikes = { createBike("SERIOUS TENAYA HERREN SCHWARZ MATT", 699.99, "Serious"),
				createBike("CROSS AVALON GENT CR", 411.99, "Cross"),
				createBike("GIANT ROAM 2 LTD (2016)", 699.99, "Giant"),
				createBike("CUBE AIM PRO BLACK'N'GREEN", 448.99, "Cube") };

		bikeDao.persist(bikes);

	}

	/**
	 * Creates Demo users.
	 */
	@Transactional
	private void createUsers() {
		System.out.println("Creating roles and inital users");
		// There is no appRoleDao, since these roles shall not be changed after
		// initialization
		final AppRole adminRole = new AppRole(Roles.ADMIN, "Administrator");
		final AppRole customerRole = new AppRole(Roles.CUSTOMER, "Customer");

		em.persist(adminRole);
		em.persist(customerRole);

		createUser("superadmin", "Super Admin", "super", adminRole, customerRole);
		createUser("admin", "Admin Admin", "admin", adminRole);
		createUser("client", "Client Client", "client", customerRole);
	}

	/**
	 * Creates a new user with given login, user name, password, and role.
	 *
	 * @param login
	 *            login
	 * @param userName
	 *            user name
	 * @param password
	 *            password
	 * @param userRoles
	 *            roles
	 * @return created user
	 */
	private AppUser createUser(String login, String userName, String password, AppRole... userRoles) {
		final AppUser user = new AppUser(login, userName);
		user.getRoles().addAll(Arrays.asList(userRoles));
		appUserDaoProxy.changePassword(user, password);
		appUserDaoProxy.persist(user);

		return user;
	}

	/**
	 * Erzeugt ein neues Bike.
	 *
	 * @param name
	 *            Bike Name
	 * @param price
	 *            Preis
	 * @param brand
	 *            Marke
	 * @return Erzeugtes Bike
	 */
	private Bike createBike(String name, double price, String brand) {
		final Bike bike = new Bike();
		bike.setName(name);
		bike.setPrice(price);
		bike.setBrand(brand);

		return bike;
	}
}
